const express = require('express');
const mysql = require('mysql');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'dieta_app'
});

connection.connect(err => {
    if (err) {
        console.error('Error al conectar con la base de datos:', err);
        return;
    }
    console.log('Conexión a la base de datos establecida.');
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/html/index.html'));
});

app.post('/guardar-usuario', (req, res) => {
    const { name, age, sex, weight, diseases, allergies, dietGoal, otherDietGoalDetails } = req.body;

    const sql = `INSERT INTO users (name, age, sex, weight, diseases, allergies, diet_goal, other_diet_goal_details) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;

    connection.query(sql, [name, age, sex, weight, diseases, allergies, dietGoal, otherDietGoalDetails], (err, results) => {
        if (err) {
            console.error('Error al insertar en la base de datos:', err);
            res.status(500).send('Error al insertar datos en la base de datos');
            return;
        }
        res.json({ message: 'Registro completado con éxito', id: results.insertId });
    });
});

app.listen(port, () => {
    console.log(`Servidor ejecutándose en http://localhost:${port}`);
});
