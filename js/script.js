function toggleOtherDietGoal() {
    var dietGoal = document.getElementById('dietGoal').value;
    var container = document.getElementById('otherDietGoalContainer');
    container.style.display = dietGoal === 'otros' ? 'block' : 'none';
}

function submitForm() {
    const form = document.getElementById('userForm');
    const formData = new FormData(form);

    const userData = {
        name: formData.get('name'),
        age: formData.get('age'),
        sex: formData.get('sex'),
        weight: formData.get('weight'),
        diseases: formData.get('diseases'),
        allergies: formData.get('allergies'),
        dietGoal: formData.get('dietGoal'),
        otherDietGoalDetails: formData.get('otherDietGoalDetails')
    };

    fetch('http://localhost:3000/guardar-usuario', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
    })
    .then(response => response.json())
    .then(data => alert('¡Registro completado!'))
    .catch(error => console.error('Error:', error));
}
