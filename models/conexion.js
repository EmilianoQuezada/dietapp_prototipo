// conexion.js

import mysql from 'mysql2';

const conexion = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "",
    database: "dieta_app"
});

conexion.connect(function(err) {
    if (err) {
        console.log("Surgió un error: " + err);
    } else {
        console.log("Se abrió la conexión con éxito");
    }
});

export default conexion;
